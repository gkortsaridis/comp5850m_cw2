package gr.gkortsaridis.opennebulaclient;

public class Helpers {

    public final static String TAG = "OPENNEBULA_LOG";

    public final static int VM_STATUS_INIT = 0;
    public final static int VM_STATUS_PENDING = 1;
    public final static int VM_STATUS_HOLD = 2;
    public final static int VM_STATUS_ACTIVE = 3;
    public final static int VM_STATUS_STOPPED = 4;
    public final static int VM_STATUS_SUSPENDED = 5;
    public final static int VM_STATUS_DONE = 6;
    public final static int VM_STATUS_FAILED = 7;
    public final static int VM_STATUS_POWEROFF = 8;
    public final static int VM_STATUS_UNDEPLOYED = 9;
    public final static int VM_STATUS_CLONING = 10;
    public final static int VM_STATUS_CLONING_FAILURE = 11;

}
