package gr.gkortsaridis.opennebulaclient;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;
import org.opennebula.client.OneResponse;

import java.util.ArrayList;
import java.util.List;

import fr.arnaudguyon.xmltojsonlib.XmlToJson;
import gr.gkortsaridis.opennebulaclient.API.AsyncTemplatePool;
import gr.gkortsaridis.opennebulaclient.API.AsyncVmPool;
import gr.gkortsaridis.opennebulaclient.API.Promise;

public class AddVMActivity extends AppCompatActivity {

    private EditText vmName;
    private Spinner vmTemplate;
    private Switch startOnHold;
    private JSONArray templates;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_vm);

        vmName = findViewById(R.id.vmname);
        vmTemplate = findViewById(R.id.vmtemplate);
        startOnHold = findViewById(R.id.startOnHold);

        AsyncTemplatePool asyncTemplatePool = new AsyncTemplatePool(this);
        asyncTemplatePool.getTempaltePoolInfo().then(new Promise.OnSuccessListener() {
            @Override
            public Object onSuccess(Object object) {
                OneResponse rc = (OneResponse) object;

                XmlToJson xmlToJson = new XmlToJson.Builder(rc.getMessage())
                        .forceList("VMTEMPLATE_POOL/VMTEMPLATE")
                        .build();

                Log.i(Helpers.TAG, xmlToJson.toJson().toString());


                JSONObject templatePoolObj = xmlToJson.toJson().optJSONObject("VMTEMPLATE_POOL");
                if (templatePoolObj != null){
                    templates = templatePoolObj.optJSONArray("VMTEMPLATE");
                    if(templates != null){

                        List<String> list = new ArrayList<String>();

                        for(int i=0; i<templates.length(); i++){
                            JSONObject template = templates.optJSONObject(i);
                            if(template != null){
                                String templateName = template.optString("NAME");
                                if(templateName != null){
                                    list.add(templateName);
                                }
                            }
                        }

                        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(AddVMActivity.this, android.R.layout.simple_spinner_item, list);
                        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        vmTemplate.setAdapter(dataAdapter);

                    }
                }

                return null;
            }
        });

    }

    public void addVM(View view){
        if(vmName.getText().toString().trim().equals("")){
            Toast.makeText(this, "Please provide a VM name", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(AddVMActivity.this, "Adding your VM. Give us a second :)", Toast.LENGTH_SHORT).show();

            AsyncVmPool asyncVmPool = new AsyncVmPool(this);

            JSONObject templ = templates.optJSONObject(vmTemplate.getSelectedItemPosition());
            if(templ != null){
                String id = templ.optString("ID");
                asyncVmPool.addVM(vmName.getText().toString(), id, startOnHold.isChecked());
                this.finish();

                Toast.makeText(this, "Successfully created VM",Toast.LENGTH_SHORT).show();
            }

        }
    }
}
