package gr.gkortsaridis.opennebulaclient;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.opennebula.client.OneResponse;

import fr.arnaudguyon.xmltojsonlib.XmlToJson;
import gr.gkortsaridis.opennebulaclient.API.AsyncGroupPool;
import gr.gkortsaridis.opennebulaclient.API.AsyncImagePool;
import gr.gkortsaridis.opennebulaclient.API.AsyncUserPool;
import gr.gkortsaridis.opennebulaclient.API.AsyncVNetPool;
import gr.gkortsaridis.opennebulaclient.API.Promise;

import static gr.gkortsaridis.opennebulaclient.Helpers.VM_STATUS_FAILED;
import static gr.gkortsaridis.opennebulaclient.Helpers.VM_STATUS_HOLD;
import static gr.gkortsaridis.opennebulaclient.Helpers.VM_STATUS_PENDING;
import static gr.gkortsaridis.opennebulaclient.MainActivity.TAG;

public class DashboardFragment extends Fragment {

    private TextView totalVMs, failedVMs, pendingVMs;
    private int totalVM_cnt, failedVM_cnt, pendingVM_cnt, ips_cnt;
    private TextView groups, users, images, gbused, vnets, ips;

    public DashboardFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);

        totalVMs = view.findViewById(R.id.totalVMs);
        failedVMs = view.findViewById(R.id.failedVMs);
        pendingVMs = view.findViewById(R.id.pendingVMs);
        groups = view.findViewById(R.id.groupsTV);
        users = view.findViewById(R.id.usersTV);
        images = view.findViewById(R.id.imagesTV);
        gbused = view.findViewById(R.id.gbusedTV);
        vnets = view.findViewById(R.id.vnetTV);
        ips = view.findViewById(R.id.ipsTV);

        totalVM_cnt = failedVM_cnt = pendingVM_cnt = ips_cnt = 0;


        JSONObject vmPoolInfo = retrieveVMPoolInfo();
        try {
            displayVMPoolInfo(vmPoolInfo);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        retrieveGroupPoolInfo().then(new Promise.OnSuccessListener() {
            @Override
            public Object onSuccess(Object object) {
                JSONObject groupPoolInfo = (JSONObject) object;
                try {
                    displayGroupPoolInfo(groupPoolInfo);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return null;
            }
        });

        retrieveUserPoolInfo().then(new Promise.OnSuccessListener() {
            @Override
            public Object onSuccess(Object object) {
                JSONObject userPoolInfo = (JSONObject) object;
                try {
                    displayUserPoolInfo(userPoolInfo);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return null;
            }
        });


        retrieveImagePoolInfo().then(new Promise.OnSuccessListener() {
            @Override
            public Object onSuccess(Object object) {
                JSONObject imagePoolInfo = (JSONObject) object;
                try {
                    displayImagePoolInfo(imagePoolInfo);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return null;
            }
        });


        retrieveVNetPoolInfo().then(new Promise.OnSuccessListener() {
            @Override
            public Object onSuccess(Object object) {
                JSONObject vnetPoolInfo = (JSONObject) object;

                try {
                    displayVNetPoolInfo(vnetPoolInfo);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return null;
            }
        });


        return view;
    }


    private void displayVMPoolInfo(JSONObject vmPoolInfo) throws JSONException {
        JSONObject vmPool = vmPoolInfo.optJSONObject("VM_POOL");

        if(vmPool != null){
            JSONArray vmList = vmPool.optJSONArray("VM");
            if(vmList != null){
                totalVM_cnt = vmList.length();

                for(int i=0; i< vmList.length(); i++){
                    JSONObject vmObj = vmList.getJSONObject(i);

                    int vmState = vmObj.getInt("STATE");

                    if(vmState == VM_STATUS_PENDING || vmState == VM_STATUS_HOLD){
                        pendingVM_cnt++;
                    }

                    if(vmState == VM_STATUS_FAILED){
                        failedVM_cnt++;
                    }

                    String ip = vmObj.optString("IP");
                    if(ip != null){
                        ips_cnt++;
                    }

                }


                totalVMs.setText(totalVM_cnt+"");
                pendingVMs.setText(pendingVM_cnt+"");
                failedVMs.setText(failedVM_cnt+"");
            }
        }
    }

    private void displayGroupPoolInfo(JSONObject groupPoolInfo) throws JSONException {
        JSONArray groupList = groupPoolInfo.getJSONObject("GROUP_POOL").getJSONArray("GROUP");

        groups.setText(groupList.length()+"");
    }

    private void displayUserPoolInfo(JSONObject userPoolInfo) throws JSONException {
        JSONArray userList = userPoolInfo.getJSONObject("USER_POOL").getJSONArray("USER");

        users.setText(userList.length()+"");
    }

    private void displayImagePoolInfo(JSONObject imagePoolInfo) throws JSONException {
        JSONArray imageList = imagePoolInfo.getJSONObject("IMAGE_POOL").getJSONArray("IMAGE");

        images.setText(imageList.length()+"");

        double totalSize = 0;
        for(int i=0; i<imageList.length(); i++){
            totalSize += imageList.getJSONObject(i).getInt("SIZE");
        }

        totalSize = totalSize/1000;

        gbused.setText(totalSize+"");

    }

    private void displayVNetPoolInfo(JSONObject vnetPoolInfo) throws JSONException {
        JSONArray vnetList = vnetPoolInfo.getJSONObject("VNET_POOL").getJSONArray("VNET");

        vnets.setText(vnetList.length()+"");
        ips.setText(ips_cnt+"");
    }


    private JSONObject retrieveVMPoolInfo(){
        String vmpoolInfo = getActivity().getIntent().getStringExtra("poolInfo");

        XmlToJson xmlToJson = new XmlToJson.Builder(vmpoolInfo)
                .forceList("/VM_POOL/VM")
                .build();
        Log.i(TAG, xmlToJson.toJson().toString());

        return xmlToJson.toJson();
    }

    private Promise retrieveGroupPoolInfo(){
        Promise promise = new Promise();
        AsyncGroupPool asyncGroupPool = new AsyncGroupPool(getActivity());
        asyncGroupPool.getGroupPoolInfo().then(new Promise.OnSuccessListener() {
            @Override
            public Object onSuccess(Object object) {
                OneResponse rc = (OneResponse) object;
                XmlToJson xmlToJson = new XmlToJson.Builder(rc.getMessage())
                        .forceList("GROUP_POOL")
                        .build();

                //Log.i(TAG, rc.getMessage());
                //Log.i(TAG, xmlToJson.toJson().toString());
                promise.resolve(xmlToJson.toJson());
                return null;
            }
        });
        return promise;
    }

    private Promise retrieveUserPoolInfo(){
        Promise promise = new Promise();

        AsyncUserPool asyncUserPool = new AsyncUserPool(getActivity());
        asyncUserPool.getUserPoolInfo().then(new Promise.OnSuccessListener() {
            @Override
            public Object onSuccess(Object object) {
                OneResponse rc = (OneResponse) object;

                XmlToJson xmlToJson = new XmlToJson.Builder(rc.getMessage())
                        .forceList("/USER_POOL/USER/GROUPS")
                        .build();

                promise.resolve(xmlToJson.toJson());
                return null;
            }
        });

        return promise;
    }

    private Promise retrieveImagePoolInfo(){
        Promise promise = new Promise();

        AsyncImagePool asyncImagePool = new AsyncImagePool(getActivity());
        asyncImagePool.getImagePoolInfo().then(new Promise.OnSuccessListener() {
            @Override
            public Object onSuccess(Object object) {
                OneResponse rc = (OneResponse) object;

                XmlToJson xmlToJson = new XmlToJson.Builder(rc.getMessage())
                        .forceList("/IMAGE_POOL/IMAGE")
                        .build();

                promise.resolve(xmlToJson.toJson());
                return null;
            }
        });

        return promise;
    }

    private Promise retrieveVNetPoolInfo(){
        Promise promise = new Promise();

        AsyncVNetPool asyncVnetPool = new AsyncVNetPool(getActivity());
        asyncVnetPool.getVNetPoolInfo().then(new Promise.OnSuccessListener() {
            @Override
            public Object onSuccess(Object object) {
                OneResponse rc = (OneResponse) object;

                XmlToJson xmlToJson = new XmlToJson.Builder(rc.getMessage())
                        .forceList("/VNET_POOL/VNET")
                        .build();

                promise.resolve(xmlToJson.toJson());
                return null;
            }
        });

        return promise;
    }

}
