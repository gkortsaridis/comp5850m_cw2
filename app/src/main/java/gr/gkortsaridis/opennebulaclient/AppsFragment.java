package gr.gkortsaridis.opennebulaclient;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONObject;
import org.opennebula.client.OneResponse;

import fr.arnaudguyon.xmltojsonlib.XmlToJson;
import gr.gkortsaridis.opennebulaclient.API.AsyncAppPool;
import gr.gkortsaridis.opennebulaclient.API.Promise;
import gr.gkortsaridis.opennebulaclient.Adapters.Adapter_AppList;


/**
 * A simple {@link Fragment} subclass.
 */
public class AppsFragment extends Fragment {

    private ListView appsList;

    public AppsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_apps, container, false);

        AsyncAppPool asyncAppPool = new AsyncAppPool(getActivity());
        asyncAppPool.getAppPoolInfo().then(new Promise.OnSuccessListener() {
            @Override
            public Object onSuccess(Object object) {
                OneResponse rc = (OneResponse) object;

                XmlToJson xmlToJson = new XmlToJson.Builder(rc.getMessage())
                        .forceList("MARKETPLACEAPP_POOL/MARKETPLACEAPP")
                        .build();

                Log.i("OPENNEBULA_LOG", xmlToJson.toJson().toString());

                JSONObject templatePoolObj = xmlToJson.toJson().optJSONObject("MARKETPLACEAPP_POOL");
                if (templatePoolObj != null){
                    JSONArray templates = templatePoolObj.optJSONArray("MARKETPLACEAPP");
                    if(templates != null){
                        appsList = view.findViewById(R.id.appslist);
                        appsList.setAdapter(new Adapter_AppList(getActivity(), templates));
                    }
                }
                return null;
            }
        });





        return view;
    }

}
