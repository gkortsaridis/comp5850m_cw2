package gr.gkortsaridis.opennebulaclient;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.jcloquell.androidsecurestorage.SecureStorage;

import org.opennebula.client.OneResponse;

import gr.gkortsaridis.opennebulaclient.API.AsyncVmPool;
import gr.gkortsaridis.opennebulaclient.API.Promise;

public class MainActivity extends AppCompatActivity {

    public final static String TAG = "OPENNEBULA_LOG";

    private EditText username,password,endpoint;
    private SecureStorage secureStorage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        username = findViewById(R.id.usernameET);
        password = findViewById(R.id.passwordET);
        endpoint = findViewById(R.id.endpointET);

        secureStorage = new SecureStorage(this, true);
    }


    public void login(View view){

        String secret = username.getText().toString()+":"+password.getText().toString();
        String endpointStr = endpoint.getText().toString();

        /*
        * There is no specific API only for logging in.
        * To test log in, we have to perform login PLUS some other operation
        * and test the second operation's result.
        * */
        AsyncVmPool asyncVmPool = new AsyncVmPool(MainActivity.this);
        asyncVmPool.getVmPoolInfo(secret,endpointStr).then(new Promise.OnSuccessListener() {
            @Override
            public Object onSuccess(Object object) {
                OneResponse rc = (OneResponse) object;

                if(rc.isError()){
                    Toast.makeText(MainActivity.this, "Authentication Failed\n"+rc.getMessage(),Toast.LENGTH_SHORT).show();
                }else{
                    secureStorage.storeObject("secret",secret);
                    secureStorage.storeObject("endpoint",endpointStr);

                    Intent intent = new Intent(MainActivity.this, DashboardMainActivity.class);
                    intent.putExtra("poolInfo",rc.getMessage());
                    startActivity(intent);
                }
                return null;
            }
        });


    }


}
