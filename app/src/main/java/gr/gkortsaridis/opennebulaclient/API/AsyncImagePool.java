package gr.gkortsaridis.opennebulaclient.API;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.jcloquell.androidsecurestorage.SecureStorage;

import org.opennebula.client.Client;
import org.opennebula.client.ClientConfigurationException;
import org.opennebula.client.OneResponse;
import org.opennebula.client.image.ImagePool;

import java.util.concurrent.ExecutionException;

public class AsyncImagePool {

    private Activity activity;
    private SecureStorage secureStorage;
    private Promise promise;

    public AsyncImagePool(Activity activity){
        this.activity = activity;
        secureStorage = new SecureStorage(activity.getApplicationContext(), true);
        promise = new Promise();
    }

    public Promise getImagePoolInfo(){
        String secret = secureStorage.getObject("secret",String.class);
        String endpoint = secureStorage.getObject("endpoint",String.class);

        new AsyncImagePool.AsyncImagePoolInfo().execute(secret, endpoint);
        return promise;
    }


    private class AsyncImagePoolInfo extends AsyncTask<String, String, OneResponse> {
        ProgressDialog p;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            p = new ProgressDialog(activity);
            p.setMessage("Please wait...");
            p.setIndeterminate(false);
            p.setCancelable(false);
            p.show();
        }

        @Override
        protected OneResponse doInBackground(String... strings) {
            String secret = strings[0];
            String endpointStr = strings[1];

            try {
                Client oneClient = new Client(secret, endpointStr);

                ImagePool imagePool = new ImagePool(oneClient);
                OneResponse rc = imagePool.infoAll();

                return rc;
            } catch (ClientConfigurationException e) {
                e.printStackTrace();

                return new OneResponse(true, e.toString());
            }
        }

        @Override
        protected void onPostExecute(OneResponse oneResponse) {
            super.onPostExecute(oneResponse);
            p.cancel();
            promise.resolve(oneResponse);
        }

    }


}
