package gr.gkortsaridis.opennebulaclient.API;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.jcloquell.androidsecurestorage.SecureStorage;

import org.opennebula.client.Client;
import org.opennebula.client.ClientConfigurationException;
import org.opennebula.client.OneResponse;
import org.opennebula.client.vm.VirtualMachinePool;
import org.opennebula.client.vnet.VirtualNetworkPool;

import java.util.concurrent.ExecutionException;

public class AsyncVNetPool {

    private Activity activity;
    private SecureStorage secureStorage;
    private Promise promise;

    public AsyncVNetPool(Activity activity){
        this.activity = activity;
        secureStorage = new SecureStorage(activity.getApplicationContext(), true);
        promise = new Promise();
    }

    public Promise getVNetPoolInfo(){
        String secret = secureStorage.getObject("secret",String.class);
        String endpoint = secureStorage.getObject("endpoint",String.class);

        new AsyncVNetPoolInfo().execute(secret, endpoint);
        return promise;
    }


    private class AsyncVNetPoolInfo extends AsyncTask<String, String, OneResponse> {
        ProgressDialog p;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            p = new ProgressDialog(activity);
            p.setMessage("Please wait...");
            p.setIndeterminate(false);
            p.setCancelable(false);
            p.show();
        }

        @Override
        protected OneResponse doInBackground(String... strings) {
            String secret = strings[0];
            String endpointStr = strings[1];

            try {
                Client oneClient = new Client(secret, endpointStr);

                VirtualNetworkPool vnetPool = new VirtualNetworkPool(oneClient);
                OneResponse rc = vnetPool.info();

                return rc;
            } catch (ClientConfigurationException e) {
                e.printStackTrace();

                return new OneResponse(true, e.toString());
            }
        }

        @Override
        protected void onPostExecute(OneResponse oneResponse) {
            super.onPostExecute(oneResponse);
            promise.resolve(oneResponse);
            p.cancel();
        }

    }

}
