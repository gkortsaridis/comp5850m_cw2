package gr.gkortsaridis.opennebulaclient.API;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import com.jcloquell.androidsecurestorage.SecureStorage;

import org.opennebula.client.Client;
import org.opennebula.client.ClientConfigurationException;
import org.opennebula.client.OneResponse;
import org.opennebula.client.host.Host;
import org.opennebula.client.host.HostPool;
import org.opennebula.client.template.Template;
import org.opennebula.client.vm.VirtualMachine;
import org.opennebula.client.vm.VirtualMachinePool;

import java.util.concurrent.ExecutionException;

import gr.gkortsaridis.opennebulaclient.Helpers;

public class AsyncVmPool {

    private Activity activity;
    private SecureStorage secureStorage;
    private ProgressDialog p;
    private Promise promise;

    public AsyncVmPool(Activity activity){
        this.activity = activity;
        secureStorage = new SecureStorage(activity.getApplicationContext(), true);
        promise = new Promise();
    }


    public Promise getVmPoolInfo(String secret, String endpoint){
        new AsyncVmPoolInfo().execute(secret, endpoint);
        return promise;
    }


    public Promise getVmPoolInfo(){
        String secret = secureStorage.getObject("secret",String.class);
        String endpoint = secureStorage.getObject("endpoint",String.class);

        new AsyncVmPoolInfo().execute(secret, endpoint);
        return promise;
    }

    public Promise addVM(String vmName, String templateId, boolean shouldHold){
        String secret = secureStorage.getObject("secret",String.class);
        String endpoint = secureStorage.getObject("endpoint",String.class);

        new AsyncAddVM().execute(secret, endpoint, templateId, vmName, shouldHold+"");
        return promise;
    }


    private class AsyncVmPoolInfo extends AsyncTask<String, String, OneResponse> {
        ProgressDialog p;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            p = new ProgressDialog(activity);
            p.setMessage("Please wait...");
            p.setIndeterminate(false);
            p.setCancelable(false);
            p.show();
        }

        @Override
        protected OneResponse doInBackground(String... strings) {
            String secret = strings[0];
            String endpointStr = strings[1];

            try {
                Client oneClient = new Client(secret, endpointStr);

                VirtualMachinePool vmPool = new VirtualMachinePool(oneClient);
                OneResponse rc = vmPool.info();

                return rc;
            } catch (ClientConfigurationException e) {
                e.printStackTrace();

                return new OneResponse(true, e.toString());
            }
        }

        @Override
        protected void onPostExecute(OneResponse oneResponse) {
            super.onPostExecute(oneResponse);
            promise.resolve(oneResponse);
            p.cancel();
        }

    }

    private class AsyncAddVM extends AsyncTask<String, String, OneResponse>{
        ProgressDialog p;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected OneResponse doInBackground(String... strings) {
            String secret = strings[0];
            String endpointStr = strings[1];
            String templateId = strings[2];
            int templateInt = Integer.parseInt(templateId);
            String vmName = strings[3];
            String shouldHold = strings[4];

            try {
                Client oneClient = new Client(secret, endpointStr);

                // Instantiating my custom Template from OpenNebula, by observing its' ID from the Website,
                // and instantiating it.
                Template template = new Template(templateInt, oneClient);
                OneResponse rc = template.instantiate(vmName);

                // This instantiates a VM from a string template
                if( rc.isError() ) {
                    Log.i(Helpers.TAG,"Creation failed!");
                    throw new Exception( rc.getErrorMessage() );
                }else{
                    Log.i(Helpers.TAG,"Creation success! VM ID:"+rc.getMessage());
                }

                // The response message is the new VM's ID
                int newVMID = Integer.parseInt(rc.getMessage());

                // We can create a representation for the new VM, using the returned
                // VM-ID
                VirtualMachine vm = new VirtualMachine(newVMID, oneClient);

                // And now we can request its information.
                rc = vm.info();
                if(rc.isError()) throw new Exception( rc.getErrorMessage() );

                Log.i(Helpers.TAG,"The new VM " + vm.getName() + " has status: " + vm.status());

                if(shouldHold.equals("false")){
                    HostPool pool = new HostPool( oneClient );
                    Integer hostId = null;
                    rc = pool.info();
                    for( Host host: pool) {
                        hostId = host.id();
                    }

                    Log.i(Helpers.TAG,"Trying to deploy the VM");
                    long startTime = System.currentTimeMillis();
                    rc = vm.deploy(hostId);
                    if(rc.isError()){
                        Log.i(Helpers.TAG,"FAILED");
                        throw new Exception(rc.getErrorMessage() );
                    }else{
                        Log.i(Helpers.TAG,"Deploy success "+rc.getMessage());
                    }

                    /*String st = vm.status();
                    while(!st.equals("runn")){
                        Thread.sleep(10);
                        vm.info();
                        st = vm.status();
                        //System.out.print(".");
                        //System.out.println("Status "+st);
                    }*/
                    long endTime = System.currentTimeMillis();
                    long elapsed = endTime - startTime;
                    Log.i(Helpers.TAG,"Instantiation time: "+elapsed+" milliseconds\n");
                }else{
                    Log.i(Helpers.TAG,"Gonna hold the VM\n");

                    rc = vm.hold();

                    if(rc.isError()){
                        Log.i(Helpers.TAG,"FAILED");
                        throw new Exception(rc.getErrorMessage() );
                    }else{
                        Log.i(Helpers.TAG,"HOLD success "+rc.getMessage());
                    }
                }

                return(rc);
            } catch (Exception e) {
                e.printStackTrace();
                return (new OneResponse(true, e.toString()));
            }
        }

        @Override
        protected void onPostExecute(OneResponse oneResponse) {
            super.onPostExecute(oneResponse);
            promise.resolve(oneResponse);
        }
    }

}
