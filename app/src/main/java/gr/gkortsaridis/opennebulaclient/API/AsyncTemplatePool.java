package gr.gkortsaridis.opennebulaclient.API;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.jcloquell.androidsecurestorage.SecureStorage;

import org.opennebula.client.Client;
import org.opennebula.client.ClientConfigurationException;
import org.opennebula.client.OneResponse;
import org.opennebula.client.template.TemplatePool;
import org.opennebula.client.vnet.VirtualNetworkPool;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class AsyncTemplatePool {

    private Activity activity;
    private SecureStorage secureStorage;
    private Promise promise;

    public AsyncTemplatePool(Activity activity){
        this.activity = activity;
        secureStorage = new SecureStorage(activity.getApplicationContext(), true);
        promise = new Promise();
    }

    public Promise getTempaltePoolInfo(){

        String secret = secureStorage.getObject("secret",String.class);
        String endpoint = secureStorage.getObject("endpoint",String.class);

        new AsyncTemplatePoolInfo().execute(secret, endpoint);
        return promise;
    }


    private class AsyncTemplatePoolInfo extends AsyncTask<String, String, OneResponse> {
        ProgressDialog p;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            p = new ProgressDialog(activity);
            p.setMessage("Please wait...");
            p.setIndeterminate(false);
            p.setCancelable(false);
            p.show();
        }

        @Override
        protected OneResponse doInBackground(String... strings) {
            String secret = strings[0];
            String endpointStr = strings[1];

            try {
                Client oneClient = new Client(secret, endpointStr);

                TemplatePool templatePool = new TemplatePool(oneClient);
                OneResponse rc = templatePool.info();

                return rc;
            } catch (ClientConfigurationException e) {
                e.printStackTrace();

                return new OneResponse(true, e.toString());
            }
        }

        @Override
        protected void onPostExecute(OneResponse oneResponse) {
            super.onPostExecute(oneResponse);
            p.cancel();
            promise.resolve(oneResponse);
        }

    }

}
