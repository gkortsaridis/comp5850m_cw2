package gr.gkortsaridis.opennebulaclient;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONObject;
import org.opennebula.client.OneResponse;

import fr.arnaudguyon.xmltojsonlib.XmlToJson;
import gr.gkortsaridis.opennebulaclient.API.AsyncVmPool;
import gr.gkortsaridis.opennebulaclient.API.Promise;
import gr.gkortsaridis.opennebulaclient.Adapters.Adapter_VMList;


public class InstancesFragment extends Fragment {

    private ListView vmList;
    private FloatingActionButton addVMBtn;

    public InstancesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();

        AsyncVmPool asyncVmPool = new AsyncVmPool(getActivity());
        asyncVmPool.getVmPoolInfo().then(new Promise.OnSuccessListener() {
            @Override
            public Object onSuccess(Object object) {
                OneResponse rc = (OneResponse) object;

                XmlToJson xmlToJson = new XmlToJson.Builder(rc.getMessage())
                        .forceList("/VM_POOL/VM")
                        .build();

                JSONObject vmPoolInfoObj = xmlToJson.toJson();
                JSONObject vmpoolObj = vmPoolInfoObj.optJSONObject("VM_POOL");
                if(vmpoolObj != null){
                    JSONArray vms = vmpoolObj.optJSONArray("VM");
                    if(vms != null){
                        vmList.setAdapter(new Adapter_VMList(getActivity(), vms));
                    }
                }

                return null;
            }
        });


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_instances, container, false);

        addVMBtn = view.findViewById(R.id.addVM);
        addVMBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), AddVMActivity.class));
            }
        });

        vmList = view.findViewById(R.id.vmlist);

        return view;
    }

}
