package gr.gkortsaridis.opennebulaclient.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import gr.gkortsaridis.opennebulaclient.R;

public class Adapter_AppList extends BaseAdapter {

    private Activity activity;
    private JSONArray apps;

    public Adapter_AppList(Activity activity, JSONArray apps){
        this.activity = activity;
        this.apps = apps;
    }

    @Override
    public int getCount() {
        return apps.length();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) activity.getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.list_item_app, parent, false);

        TextView id = rowView.findViewById(R.id.tempIDTV);
        TextView name = rowView.findViewById(R.id.tempNameTV);
        TextView uname = rowView.findViewById(R.id.tempUnameTV);

        try {
            JSONObject app = apps.getJSONObject(position);


            String id_str = app.optString("ID");
            String name_str = app.optString("NAME");
            Integer size_int = app.optInt("SIZE");

            if(id_str != null && !id_str.equals("")){ id.setText(id_str);
            }else{ id.setText("-"); }

            if(name_str != null && !name_str.equals("")){ name.setText(name_str);}
            else { name.setText("-");}

            if(size_int != null){
                Double sizeD = (size_int.doubleValue())/1000;
                uname.setText(sizeD+"");
            }
            else {uname.setText("----");}

        } catch (JSONException e) {
            e.printStackTrace();
        }





        return rowView;
    }
}
