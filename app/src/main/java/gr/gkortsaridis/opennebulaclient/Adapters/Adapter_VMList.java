package gr.gkortsaridis.opennebulaclient.Adapters;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import gr.gkortsaridis.opennebulaclient.Helpers;
import gr.gkortsaridis.opennebulaclient.R;

public class Adapter_VMList extends BaseAdapter {

    private Activity activity;
    private JSONArray vms;

    public Adapter_VMList(Activity activity, JSONArray vms){
        this.activity = activity;
        this.vms = vms;
    }

    @Override
    public int getCount() {
        return vms.length();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) activity.getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.list_item_vm, parent, false);

        TextView id = rowView.findViewById(R.id.vmIDTV);
        TextView name = rowView.findViewById(R.id.vmNameTV);
        TextView ip = rowView.findViewById(R.id.vmIPTV);

        try {
            JSONObject vm = vms.getJSONObject(position);


            Log.i(Helpers.TAG, vm.toString());


            String id_str = vm.optString("ID");
            String name_str = vm.optString("NAME");
            Integer state = vm.optInt("STATE");

            if(id_str != null && !id_str.equals("")){ id.setText(id_str);
            }else{ id.setText("-"); }

            if(name_str != null && !name_str.equals("")){ name.setText(name_str);}
            else { name.setText("-");}

            if(state != null){
                ip.setText(stateToString(state));
            }
            else {ip.setText("----");}

        } catch (JSONException e) {
            e.printStackTrace();
        }





        return rowView;
    }

    private String stateToString(int state){
        if(state == 0){ return "INIT"; }
        else if(state == 1){ return "PENDING"; }
        else if(state == 2){ return "HOLD"; }
        else if(state == 3){ return "ACTIVE"; }
        else if(state == 4){ return "STOPPED"; }
        else if(state == 5){ return "SUSPENDED"; }
        else if(state == 6){ return "DONE"; }
        else if(state == 7){ return "ERROR"; }
        else if(state == 8){ return "POWEROFF"; }
        else if(state == 9){ return "UNDEPLOYED"; }
        else if(state == 10){ return "CLONING"; }
        else if(state == 11){ return "CLONING_FAILURE"; }
        else return "??";
    }
}
