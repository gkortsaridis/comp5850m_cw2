package gr.gkortsaridis.opennebulaclient;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONObject;
import org.opennebula.client.OneResponse;

import fr.arnaudguyon.xmltojsonlib.XmlToJson;
import gr.gkortsaridis.opennebulaclient.API.AsyncTemplatePool;
import gr.gkortsaridis.opennebulaclient.API.Promise;
import gr.gkortsaridis.opennebulaclient.Adapters.Adapter_TemplateList;

public class TemplatesFragment extends Fragment {

    private ListView templateList;

    public TemplatesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_templates, container, false);


        AsyncTemplatePool asyncTemplatePool = new AsyncTemplatePool(getActivity());
        asyncTemplatePool.getTempaltePoolInfo().then(new Promise.OnSuccessListener() {
            @Override
            public Object onSuccess(Object object) {
                OneResponse rc = (OneResponse) object;

                XmlToJson xmlToJson = new XmlToJson.Builder(rc.getMessage())
                        .forceList("VMTEMPLATE_POOL/VMTEMPLATE")
                        .build();

                Log.i("OPENNEBULA_LOG", xmlToJson.toJson().toString());

                JSONObject templatePoolObj = xmlToJson.toJson().optJSONObject("VMTEMPLATE_POOL");
                if (templatePoolObj != null){
                    JSONArray templates = templatePoolObj.optJSONArray("VMTEMPLATE");
                    if(templates != null){
                        templateList = view.findViewById(R.id.templatelist);
                        templateList.setAdapter(new Adapter_TemplateList(getActivity(), templates));
                    }
                }

                return null;
            }
        });






        return view;
    }

}
